FROM php:7.2.8

RUN apt-get update

RUN apt-get install -y zip curl gcc make git libpq-dev zlib1g-dev

RUN docker-php-ext-install pdo_pgsql pgsql zip

#install composer
ENV COMPOSER_ALLOW_SUPERUSER=1

RUN curl -sS https://getcomposer.org/installer | php \
  && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer

RUN composer global require "laravel/installer"

ENV PATH="/root/.composer/vendor/bin:${PATH}"

